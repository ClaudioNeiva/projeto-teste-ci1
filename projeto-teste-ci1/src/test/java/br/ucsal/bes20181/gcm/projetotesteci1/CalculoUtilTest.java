package br.ucsal.bes20181.gcm.projetotesteci1;

import org.junit.Assert;
import org.junit.Test;

public class CalculoUtilTest {

	@Test
	public void calcularFatorial0Test() {
		Long n = 0L;
		Long fatorialEsperado = 1L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
