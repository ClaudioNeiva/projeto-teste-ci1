package br.ucsal.bes20181.gcm.projetotesteci1;

public class CalculoUtil {

	public static Long calcularFatorial(Long n) {
		Long fat = 0L;
		for (Long i = 1L; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

}
